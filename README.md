# Part 1 - Development

## Project Brief

* You are tasked with producing an HTML version of the mockup in the `assets` folder
* The page should be responsive to all common screen sizes (Bootstrap breakpoints are fine)
* "Pixel perfection" is **not** a requirement, but it should look as close to the mockup as reasonably possible
* [Bootstrap](https://getbootstrap.com/) should be used as the base css framework (*already included in `index.html`*)
* Feel free to submit your work even if you do not finish all the tasks, as you will be evaluated on your approach to solving the challenges presented by the mockup, along with code quality, and not by quantity. However, the more sections you finish, the more code samples we'd have to accurately assess your performance.

### Starting Development

* Fonts required for the mockup are available in the `assets/fonts` folder.
* Mockup files in Figma format is available in the `assets` folder. (Use [Figma](https://figma.com) to open the file.)
* For your convenience, [Bootstrap 5](https://getbootstrap.com/) and [Google Material Symbols](hhttps://fonts.google.com/icons)(https://developers.google.com/fonts/docs/material_symbols) have been added to the `index.html` file.
* The Branch Locator section has the basic html structure with some helpful data added to the `index.html` file to assist you in completing this section. You will have to create the script to add the functionality of changing the map according to the selected address, style it to match the mockup, and turn it responsive.

# Part 2 - Design

## Project Brief

* Please design two companion banner advertisements that pair with the website mockup
* Use whatever design tool you feel most comfortable with
* Both sizes can be the based on the same creative style you decide on
* Should match the look and feel of the website mockup
* Use your best design judgement on which information to display (see below). Not all information is required and is provided as a guide
* Sizes:
  * Size 1: Wide Skyscraper (160px by 600px)
  * Size 2: Large Rectangle (336px by 280px)

### Information to Display:
  * **Company Logo** (from website mockup file)
  * **Main Message:** AC & Furnace Installation Service
  * **Secondary Message:** Friendly & Licensed Technicians
  * **CTA:** Call Us Today
  * **Phone Number:** 555-555-5555
  * **Imagery:** An AC or furnace unit.

### Saving Your Designs
* Save the banners in JPG format inside the `2-design` folder

# When You're Finished Both Parts
* Zip up the finished project folders (`1-development` and `2-design`)
* Send the zip file back to the person who provided you this assignment

# Questions?
We are happy to answer any questions you have during this process, so don't be shy! Reach out to the person who provided you this assignment.